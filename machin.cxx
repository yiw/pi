/*
 * Copyright 2020 Yoshiyasu Iwasaki

 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#include <iostream>
#include <thread>
#include <future>
#include <algorithm>
#include <vector>
#include <string>
#include <chrono>
#include <fstream>
#include <cmath>
#include <gmpxx.h>

using namespace std;

// arctan(1/x)
mpz_class parallel_arctan(unsigned id, unsigned nthreads,
                          mpz_class x, const mpz_class one, unsigned n)
{
    auto start = id;
    auto step = nthreads;
    auto step2 = step * 2;
    mpz_class x_pow;
    mpz_pow_ui(x_pow.get_mpz_t(), x.get_mpz_t(), step2);
    mpz_class divisor{2 * start + 1};
    mpz_pow_ui(x.get_mpz_t(), x.get_mpz_t(), divisor.get_ui());
    x = one / x;
    if (start % 2)
        x = -x;
    mpz_class y{x / divisor};
    start += step;
    if (step % 2) {
        for (auto i = start; i < n; i += step) {
            divisor += step2;
            x /= -x_pow;
            y += x / divisor;
        }
    } else {
        for (auto i = start; i < n; i += step) {
            divisor += step2;
            x /= x_pow;
            y += x / divisor;
        }
    }
    return y;
}

mpz_class machin(unsigned n, unsigned ndigits, unsigned nthreads)
{
    if (n < 1) {
        mpz_class zero{0};
        return zero;
    }
    mpz_class one;
    mpz_ui_pow_ui(one.get_mpz_t(), 10, ndigits);
    mpz_class x1{5};
    vector<future<mpz_class>> y1;
    for (auto i = 0; i < nthreads; i++)
        y1.emplace_back(async(launch::async, parallel_arctan, i, nthreads, x1, one, n));
    mpz_class z1{0};
    mpz_class x2{239};
    vector<future<mpz_class>> y2;
    unsigned i = 0;
    for (auto &&y: y1) {
        z1 += y.get();
        y2.emplace_back(async(launch::async, parallel_arctan, i, nthreads, x2, one, n));
        i++;
    }
    mpz_class z2{0};
    for (auto &&y: y2)
        z2 += y.get();
    return 16 * z1 - 4 * z2;
}

int main()
{
    unsigned ndigits = 1000000; // <- Change this value
    cout << "The number of digits = " << ndigits << endl;
    double log10_5 = log10(5.0);
    auto n = static_cast<unsigned>(ceil((ndigits - log10_5) / (2.0 * log10_5)));
    cout << "the number of terms = " << n << endl;
    // The default number of threads is the number of threads of your CPU.
    unsigned nthreads = thread::hardware_concurrency();
    cout << "the number of threads = " << nthreads << endl;

    cout << endl;
    cout << "Computing pi. It may take a very long time." << endl;
    auto start = chrono::system_clock::now();
    mpz_class pi{machin(n, ndigits, nthreads)};
    auto end = chrono::system_clock::now();
    cout << "Done." << endl;

    auto elapsed = chrono::duration_cast<chrono::milliseconds>(end - start);
    cout << endl;
    cout << "time = " << elapsed.count() / 1000.0 << " seconds" << endl;

    cout << endl;
    cout << "Writing to file." << endl;
    ofstream f;
    f.open("pi.txt", ios::out);
    const string s{pi.get_str()};
    f << s[0] << '.';
    for (size_t i = 1; s[i]; i++) {
        if (i % 5 == 1) {
            if (i % 50 == 1)
                f << endl;
            else
                f << " ";
        }
        f << s[i];
    }
    f << endl;
    cout << "Done." << endl;

    return 0;
}
